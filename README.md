# The project of financial analysis is a learning journey to become a financial analyst welded with quantitative technics

## Introduction

This article is tended to implement the concept of Warren Beffett from his book \"Warren Buffett and the Interpretation of Financial Statements\". I would try to describe the fundamental theory as well as visualize the results. The ultimate purpose is to compare with different methods and figure out how to analyze financial statements in a proper manner. If your goal is to make money, then that is not under this article scope.

## Read through some of my financial tutorials on Youtube or Medium!
https://www.youtube.com/watch?v=VBXALP8VhLQ&ab_channel=AlphaGo
[![IMAGE ALT TEXT](image/CAPE.png)](https://www.youtube.com/watch?v=VBXALP8VhLQ&ab_channel=AlphaGo)

https://anfangen.medium.com/basic-income-statement-exploration-fd534aba7533
[![IMAGE ALT FILM](image/Allianz.jpeg)](https://anfangen.medium.com/basic-income-statement-exploration-fd534aba7533)

## How to tweak this project for your own uses

I'd encourage you to clone this project to use for your own puposes. It's a good starter to start a journey to become a financial analyst

## Known issues (Work in progress)

The yahoo finance API is updating. Thus, it has higher restriction to access the financial data. Better need to check out some alternative work around to obtain those fincancial KPIs.  

## Like this project?

If you are feeling generous, please click star button and feel free to drop some messages.
